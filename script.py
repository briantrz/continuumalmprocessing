import re, openpyxl, os
import pandas as pd

files = os.listdir()
dmpFiles = []

keys = {'name': 'name', 'Type': 'EventType', 'Reset message': 'MsgTextNormal', 'Category': 'EventNotificationId'}

### Define patterns to be replaced
patterns = [
  {'match': '%d', 'replace': '@(MonitoredObject->DESCR)'},
  {'match': '%D', 'replace': '@(MonitoredObject->DESCR)'}]

### Find any .dmp files in the same directory as the script
for file in files:
  if '.dmp' in file:
    dmpFiles.append(file)

### Open the .dmp file and read it in as a string
for dmpFile in dmpFiles:
  with open(dmpFile, 'r') as inFile:
    dmpString = inFile.read()

### Replace any patterns defined in the list
  for pattern in patterns:
    dmpString = re.sub(pattern['match'], pattern['replace'], dmpString)

  AlarmObjects = {}

### Find any folders in the .dmp file
  folders = re.findall('BeginContainer : ((\S)+?)\n((.|\n)+?)EndContainer', dmpString)

  for folder in folders:
    AlarmObjects[folder[0]] = {}
    alarms = re.findall('Object : ((\S)+?)\n((.|\n)+?)EndObject', folder[2])
    for alarm in alarms:
      AlarmObjects[folder[0]][alarm[0]] = {}
      propDict = {'name': alarm[0]}
      properties = re.findall('((\S)+?) : ((.)+?)\n', alarm[2])
      for prop in properties:
        propDict[prop[0]] = prop[2]
      if 'EventType' in propDict:
        for key in keys:
          if keys[key] in propDict:
            AlarmObjects[folder[0]][alarm[0]][key] = propDict[keys[key]]
        if 'HiLimit' in propDict['EventType']:
          AlarmObjects[folder[0]][alarm[0]]['Type'] = 'infinity.HighLimit'
          AlarmObjects[folder[0]][alarm[0]]['High limit'] = propDict['Limit']
          AlarmObjects[folder[0]][alarm[0]]['Above upper limit message'] = propDict['MsgTextOffNormal']
          AlarmObjects[folder[0]][alarm[0]]['Return to normal'] = propDict['Normal']
        elif 'LowLimit' in propDict['EventType']:
          AlarmObjects[folder[0]][alarm[0]]['Type'] = 'infinity.LowLimit'
          AlarmObjects[folder[0]][alarm[0]]['Low limit'] = propDict['Limit']
          AlarmObjects[folder[0]][alarm[0]]['Below lower limit message'] = propDict['MsgTextOffNormal']
          AlarmObjects[folder[0]][alarm[0]]['Return to normal'] = propDict['Normal']
        elif 'ACCExpression' in propDict['EventType']:
          AlarmObjects[folder[0]][alarm[0]]['Type'] = 'infinity.Expression'
          AlarmObjects[folder[0]][alarm[0]]['ExpressionString'] = propDict['ACCExpression']
          AlarmObjects[folder[0]][alarm[0]]['AlarmMessage'] = propDict['MsgTextOffNormal']

  for folder in AlarmObjects:
    almFolder = []
    fileName = folder + '.xlsx'
    for almObjs in AlarmObjects[folder]:
      almFolder.append(AlarmObjects[folder][almObjs])
  
    df = pd.DataFrame(almFolder, index=None)
    df.to_excel(fileName)